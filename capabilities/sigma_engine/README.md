# Overview
The sigma engine is a core piece of the Pantheon platform, providing it with the necessary core features of a SIEM, 
alerting on Sigma based rules. The platform has little value if it cannot reproduce basic SIEM like capabilities 
alongside its additional features.

The Sigma Engine component is backed by ElastAlert, that doesnt use the standard set of Pantheon APIs to interact with 
the data but interacts with the data at rest directly. [The HELK Project](https://github.com/Cyb3rWard0g/HELK) has 
already containerised the component we require into a docker container that is deployed on the _Elastic Master Node_, 
not in the capability cluster (another slightly off design item to note) which has provided most of the codebase here

In order for the ElastAlert component to write to the alerting topic, a Kafka alerter needs to be written, which has 
ben [done here](https://github.com/windhamwong/elastalert_kafka)