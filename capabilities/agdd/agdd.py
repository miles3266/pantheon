# This import breaks on windows
# Backed by flare: https://github.com/austin-taylor/flare
from flare.data_science.features import dga_classifier

from pantheon_api.stream import KafkaProducer, KafkaConsumer


def main():
    dga_c = dga_classifier()
    consumer = KafkaConsumer('packetbeat')  # Topic to consume from
    producer = KafkaProducer
    for message in consumer.consume:
        if dga_c.predict(message['server']) == 'dga':
            #  Example alert type
            message['alert'] = [{"alert": "true"}, {"type": "agdd"},
                                {"message": "The AGDD component detected this as a potentially algorithmically "
                                            "generated domain name"},
                                {"severity", "medium"}]
            producer.send(message)  # only forward the message if there is an alert


if __name__ == '__main__':
    main()
