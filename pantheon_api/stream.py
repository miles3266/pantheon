import requests
from kafka import KafkaConsumer, KafkaProducer

# TODO inject this from config somewhere
pantheon_master = "https://panthon-master.pantheon-master-1.europe-west4-a.c.ares-pantheon.internal"

'''
The Kafka Consumer is the first interface that any capability needs to implement. It takes a topic to consume from as a
parameter, and uses that to interrogate the configuration store on the pantheon master. This returns the required 
group_id of the topic, as well as the locations of the kafka brokers for it to consume from.
'''


class KafkaConsumer:

    # Something like this idk yet
    def __init__(self, topic):
        self.topic = topic
        self.group_id = requests.get(f"{pantheon_master}/configuration/consumers/groups/{self.topic}").json()
        self.servers = requests.get(f"{pantheon_master}/configuration/consumers/servers").json()
        self.consumer = KafkaConsumer(self.topic,
                                      group_id=self.group_id['group_id'],
                                      bootstrap_servers=[self.servers['server']])

    def consume(self):
        return self.consumer


'''
The Kafka Producer is the second required interface to be implemented, wrapping around the alert queue producer
'''


class KafkaProducer:

    def __init__(self):
        self.topic = 'stream_alerts'
        self.servers = requests.get(f"{pantheon_master}/configuration/producers/servers").json()
        self.producer = KafkaProducer(bootstrap_servers=self.servers['server'])

    def send(self, message):
        # TODO there should probably be some kind of input validation on the message
        self.producer.send(self.topic, message)
