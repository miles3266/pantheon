#!/usr/bin/env bash
install_puppet_agent() {
    rpm -Uvh https://yum.puppet.com/puppet5/puppet5-release-el-7.noarch.rpm	
    yum install -y epel-release
    yum install -y puppet-agent
    source /etc/profile
}

configure_puppet_agent(){
    config = $$(puppet config print config)
    cat > "$${config}" <<EOF
[main]
server = $$1
EOF
}

install_puppet_agent
configure_puppet_agent "pantheon-master-1.europe-west4-a.c.ares-pantheon.internal"
puppet agent -t
