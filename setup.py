from setuptools import setup

setup(name='pantheon',
      version='0.1.0',
      description='Pantheon Master',
      url='https://gitlab.com/miles3266/pantheon',
      license='Apache-2.0',
      author='Dan Miles',
      packages=['pantheon', 'pantheon_api'],
      install_requires=['flask', 'google-api-python-client', 'flask_restful', 'requests', 'kafka-python', 'ZODB'])
