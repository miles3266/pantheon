"""
Basic representation of what a project blueprints would need to look like
"""
import persistent


class ProjectBlueprints(persistent.Persistent):

    def __init__(self, blueprints):
        self.project_id = blueprints['project_id']
        self.project_owner = blueprints['project_owner']
        self.provider = blueprints['provider']
        self.ingress_rate = blueprints['ingress_rate']  # Can be an estimate
        self.core_instances = blueprints['core']['instances']  # List of instances
        self.core_state = 'staged'
        self.capability_clusters = blueprints['capability']['clusters']  # List of Kubernetes clusters
        self.capabilities = blueprints['capability']['capabilities']  # List of capability Ids
