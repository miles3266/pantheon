import logging
import sys

from googleapiclient.discovery import build

from pantheon.provisioners.abstract import ComputeProvisioner

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


class CloudCompute(ComputeProvisioner):

    def __init__(self):
        self.compute_api = build('compute', 'v1')
        self.startup_script = "bash <(curl -s https://gitlab.com/miles3266/pantheon/raw/master/scripts/gcp_provisioner.sh)"

    def create_instance(self, machine_role, name, project_id):
        zone = "europe-west4-a"
        # TODO make zone configured

        if machine_role == "front-door":
            logging.info("CloudCompute: provisioning front door on gcp")
            config = self._configure_front_door(name, zone)
        elif machine_role == "datalake":
            logging.info("CloudCompute: provisioning datalake node on gcp")
            config = self._configure_datalake(name, zone)
        elif machine_role == "queue":
            logging.info("CloudCompute: provisioning queue server on gcp")
            config = self._configure_queue(name, zone)
        else:
            logging.info("CloudCompute: provisioning default compute on gcp")
            config = self._configure_default(name, zone)

        result = self.compute_api.instances().insert(project=project_id,
                                                     zone=zone,
                                                     body=config).execute()
        if result['status'] == "DONE":
            return "provisioned"
        else:
            return result['error']

    def delete_instance(self, instance_name, project_id):
        zone = "europe-west4-a"
        result = self.compute_api.instances().delete(project=project_id,
                                                     zone=zone,
                                                     instance=instance_name).execute()
        logging.info(str(result))
        return "deleted"

    # TODO there is still a lot of project specific configuration injection to
    # be done here
    def _configure_front_door(self, machine_name, zone):
        config = {
            "name": machine_name,
            "zone": zone,
            "machineType": "zones/%s/machineTypes/n1-standard-1" % zone,
            "metadata": {
                "items": [{
                    'key': 'startup-script',
                    'value': self.startup_script
                }]
            },
            "tags": {
              "items": [
                "frontdoor"
              ]
            },
            "disks": [
                {
                    "kind": "compute#attachedDisk",
                    "type": "PERSISTENT",
                    "boot": True,
                    "mode": "READ_WRITE",
                    "autoDelete": True,
                    "deviceName": machine_name,
                    "initializeParams": {
                        "sourceImage": "projects/centos-cloud/global/images/centos-7-v20190116",
                        "diskType": "projects/ares-pantheon/zones/europe-west4-a/diskTypes/pd-standard",
                        "diskSizeGb": "10"
                    }
                }
            ],
            "canIpForward": False,
            "networkInterfaces": [
                {
                    "kind": "compute#networkInterface",
                    "subnetwork": "projects/ares-pantheon/regions/europe-west4/subnetworks/default",
                    "accessConfigs": [
                        {
                            "kind": "compute#accessConfig",
                            "name": "External NAT",
                            "type": "ONE_TO_ONE_NAT",
                            "networkTier": "PREMIUM"
                        }
                    ],
                    "aliasIpRanges": []
                }
            ],
            "description": "",
            "labels": {},
            "scheduling": {
                "preemptible": False,
                "onHostMaintenance": "MIGRATE",
                "automaticRestart": True,
                "nodeAffinities": []
            },
            "deletionProtection": False,
            "serviceAccounts": [
                {
                    "email": "61533729081-compute@developer.gserviceaccount.com",
                    "scopes": [
                        "https://www.googleapis.com/auth/devstorage.read_only",
                        "https://www.googleapis.com/auth/logging.write",
                        "https://www.googleapis.com/auth/monitoring.write",
                        "https://www.googleapis.com/auth/servicecontrol",
                        "https://www.googleapis.com/auth/service.management.readonly",
                        "https://www.googleapis.com/auth/trace.append"
                    ]
                }
            ]
        }
        return config

    def _configure_datalake(self, machine_name, zone):
        config = {
            "name": machine_name,
            "zone": zone,
            "machineType": "zones/%s/machineTypes/n1-highmem-2" % zone,
            "metadata": {
              "kind": "compute#metadata",
              "items": [
                {
                  "key": "startup-script",
                  "value": self.startup_script
                }
              ]
            },
            "tags": {
              "items": [
                "datalake"
              ]
            },
            "disks": [
              {
                "kind": "compute#attachedDisk",
                "type": "PERSISTENT",
                "boot": True,
                "mode": "READ_WRITE",
                "autoDelete": True,
                "deviceName": machine_name,
                "initializeParams": {
                  "sourceImage": "projects/centos-cloud/global/images/centos-7-v20190116",
                  "diskType": "projects/ares-pantheon/zones/europe-west4-a/diskTypes/pd-standard",
                  "diskSizeGb": "10"
                }
              },
              {
                "kind": "compute#attachedDisk",
                "mode": "READ_WRITE",
                "autoDelete": False,
                "type": "PERSISTENT",
                "initializeParams": {
                  "diskType": "projects/ares-pantheon/zones/europe-west4-a/diskTypes/pd-standard",
                  "diskSizeGb": "200",
                  "description": "Data disk for the datalake"
                }
              }
            ],
            "canIpForward": False,
            "networkInterfaces": [
              {
                "kind": "compute#networkInterface",
                "network": "projects/ares-pantheon/global/networks/default",
                "aliasIpRanges": []
              }
            ],
            "labels": {},
            "scheduling": {
              "preemptible": False,
              "onHostMaintenance": "MIGRATE",
              "automaticRestart": True,
              "nodeAffinities": []
            },
            "serviceAccounts": [
              {
                "email": "61533729081-compute@developer.gserviceaccount.com",
                "scopes": [
                  "https://www.googleapis.com/auth/devstorage.read_only",
                  "https://www.googleapis.com/auth/logging.write",
                  "https://www.googleapis.com/auth/monitoring.write",
                  "https://www.googleapis.com/auth/servicecontrol",
                  "https://www.googleapis.com/auth/service.management.readonly",
                  "https://www.googleapis.com/auth/trace.append"
                ]
              }
            ]
        }
        return config

    def _configure_default(self, machine_name, zone):
        config = {
            "name": machine_name,
            "zone": zone,
            "machineType": "zones/%s/machineTypes/n1-standard-4" % zone,
            "metadata": {
              "kind": "compute#metadata",
              "items": [
                {
                  "key": "startup-script",
                  "value": self.startup_script
                }
              ]
            },
            "tags": {
              "items": [
                "default"
              ]
            },
            "disks": [
              {
                "kind": "compute#attachedDisk",
                "type": "PERSISTENT",
                "boot": True,
                "mode": "READ_WRITE",
                "autoDelete": True,
                "deviceName": machine_name,
                "initializeParams": {
                  "sourceImage": "projects/centos-cloud/global/images/centos-7-v20190116",
                  "diskType": "projects/ares-pantheon/zones/europe-west4-a/diskTypes/pd-standard",
                  "diskSizeGb": "10"
                }
              },
              {
                "kind": "compute#attachedDisk",
                "mode": "READ_WRITE",
                "autoDelete": False,
                "type": "PERSISTENT",
                "initializeParams": {
                  "diskType": "projects/ares-pantheon/zones/europe-west4-a/diskTypes/pd-standard",
                  "diskSizeGb": "50",
                  "description": "Data disk for the datalake"
                }
              }
            ],
            "canIpForward": False,
            "networkInterfaces": [
              {
                "kind": "compute#networkInterface",
                "network": "projects/ares-pantheon/global/networks/default",
                "aliasIpRanges": []
              }
            ],
            "labels": {},
            "scheduling": {
              "preemptible": False,
              "onHostMaintenance": "MIGRATE",
              "automaticRestart": True,
              "nodeAffinities": []
            },
            "serviceAccounts": [
              {
                "email": "61533729081-compute@developer.gserviceaccount.com",
                "scopes": [
                  "https://www.googleapis.com/auth/devstorage.read_only",
                  "https://www.googleapis.com/auth/logging.write",
                  "https://www.googleapis.com/auth/monitoring.write",
                  "https://www.googleapis.com/auth/servicecontrol",
                  "https://www.googleapis.com/auth/service.management.readonly",
                  "https://www.googleapis.com/auth/trace.append"
                ]
              }
            ]
          }
        return config

    def _configure_queue(self, machine_name, zone):
        config = {
            "name": machine_name,
            "zone": zone,
            "machineType": "zones/%s/machineTypes/n1-highcpu-4" % zone,
            "metadata": {
              "kind": "compute#metadata",
              "items": [
                {
                  "key": "startup-script",
                  "value": self.startup_script
                }
              ]
            },
            "tags": {
              "items": [
                "queue"
              ]
            },
            "disks": [
              {
                "kind": "compute#attachedDisk",
                "type": "PERSISTENT",
                "boot": True,
                "mode": "READ_WRITE",
                "autoDelete": True,
                "deviceName": "datalake-1",
                "initializeParams": {
                  "sourceImage": "projects/centos-cloud/global/images/centos-7-v20190116",
                  "diskType": "projects/ares-pantheon/zones/europe-west4-a/diskTypes/pd-standard",
                  "diskSizeGb": "10"
                }
              },
              {
                "kind": "compute#attachedDisk",
                "mode": "READ_WRITE",
                "autoDelete": False,
                "type": "PERSISTENT",
                "initializeParams": {
                  "diskType": "projects/ares-pantheon/zones/europe-west4-a/diskTypes/pd-standard",
                  "diskSizeGb": "50",
                  "description": "Data disk for the datalake"
                }
              }
            ],
            "canIpForward": False,
            "networkInterfaces": [
              {
                "kind": "compute#networkInterface",
                "network": "projects/ares-pantheon/global/networks/default",
                "aliasIpRanges": []
              }
            ],
            "labels": {},
            "scheduling": {
              "preemptible": False,
              "onHostMaintenance": "MIGRATE",
              "automaticRestart": True,
              "nodeAffinities": []
            },
            "serviceAccounts": [
              {
                "email": "61533729081-compute@developer.gserviceaccount.com",
                "scopes": [
                  "https://www.googleapis.com/auth/devstorage.read_only",
                  "https://www.googleapis.com/auth/logging.write",
                  "https://www.googleapis.com/auth/monitoring.write",
                  "https://www.googleapis.com/auth/servicecontrol",
                  "https://www.googleapis.com/auth/service.management.readonly",
                  "https://www.googleapis.com/auth/trace.append"
                ]
              }
            ]
          }
        return config


class CloudContainerEngine:

    def __init__(self):
        self.engine = build('container', 'v1')

    def _create_cluster(self):
        self.engine.clusters().create().execute()  # not a fucking clue?!

        return "todo"

    def _deploy_workload(self):
        return "todo"
