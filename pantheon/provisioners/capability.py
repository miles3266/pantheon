from flask_restful import Resource


# A fancy name for a kubernetes cluster provisioner
class CapabilityManagerProvisioner(Resource):

    def get(self):
        # returns a list of the currently provisioned clusters, and their node
        # counts
        return "todo"

    def post(self, provider):
        # Depending on the provider, passed as a parameter, this should wrap
        # around the appropriate provider APIs to provision the kubernetes
        # cluster
        return "todo"


# A wrapper around the API's required to deploy a container to the cluster
class CapabilityProvisioner(Resource):

    def get(self):
        # should return a list of currently deployed capabilities
        return "todo"

    def post(self, capability_id):
        # This method takes a single capability id as a parameter, and invokes
        # the required APIs to lookup the
        # capability in the registry, and deploy it to a capability manager
        # cluster for that platform.
        #
        # As part of the deployment, it needs to get the required configuration
        # to inject into the container
        return "todo"

    def delete(self, capability_id):
        # Should a user no longer want a capability
        return "todo"
