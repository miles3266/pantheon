
class ComputeProvisioner:

    def create_instance(self, machine_role, name, project_id):
        raise Exception("Cannot Call method on abstract class")

    def delete_instance(self, instance_name, project_id):
        raise Exception("Cannot Call method on abstract class")
