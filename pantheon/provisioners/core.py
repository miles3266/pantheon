import logging
import sys

from flask import request
from flask_restful import Resource

from pantheon.model.blueprints import ProjectBlueprints
from pantheon.persistence import DatabaseManager
from pantheon.provisioners.abstract import ComputeProvisioner
from pantheon.provisioners.googlecloud import CloudCompute

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


class CoreDetails(Resource):

    def get(self, project_id):
        try:
            dbm = DatabaseManager()
            logging.debug("Core: got database manager")
            conn = dbm.get_connection()
            logging.debug("Core: got connection")
            projects = conn.root.projects
            project = projects[project_id]
            return {project_id: str(project.core_instances)}
        except Exception as e:
            logging.warnning(e)
            return {"message": "error getting project"}, 502


class CoreProvisioner(Resource):

    def __init__(self):
        self.compute_provisioner = ComputeProvisioner

    def post(self):
        blueprints = ProjectBlueprints(request.get_json(force=True))
        logging.info("Core: received blueprints: " + str(blueprints))

        # get all required instances
        if int(blueprints.ingress_rate) > 0:  # Appends to any custom instances that are required
            logging.info("scaling core based on ingress rate of: " + blueprints.ingress_rate)
            blueprints.core_instances.append(self._scale(blueprints.ingress_rate))

        # determine which provider to use
        if blueprints.provider == "gcp":
            logging.info("using gcp provider")
            self.compute_provisioner = CloudCompute()
        if blueprints.provider == "aws":
            logging.info("using aws provider")
            self.compute_provisioner = ComputeProvisioner  # TODO implement

        # start provioning process
        logging.info("Core: instances to build: " + str(blueprints.core_instances))
        blueprints.core_state = "provisioned"
        for i, instance in enumerate(blueprints.core_instances):
            try:
                logging.info("provisioning: " + str(instance))
                result = self.compute_provisioner.create_instance(
                    instance['role'], instance['name'], blueprints.project_id)
                instance['state'] = result
                logging.info(str(instance['state']))
            except Exception as error:
                logging.warning(error)
                # TODO perform a health check to aggregate the issues
                blueprints.core_state = "tainted"
                try:
                    instance['state'] = error
                except Exception as nested_exception:
                    blueprints.core_state = "tainted-n"
                    logging.error(nested_exception)
                    continue

        dbm = DatabaseManager()
        logging.debug("Core: got database manager")
        conn = dbm.get_connection()
        logging.debug("Core: got connection")
        # This is still exposing ZODB specific APIs which I'd like to hide
        conn.root.projects[blueprints.project_id] = blueprints
        dbm.commit(conn)
        return {"message": blueprints.core_state}

    # TODO implement
    def _scale(self, ingress_rate):
        if ingress_rate >= 40000:
            required_instances = [
                {"name": "front-door-1", "role": "front-door"},  # Firewall configured accordingly
                {"name": "normaliser-1", "role": "compute"},
                {"name": "elastic-1", "role": "datalake"},  # Elastic 1 has very high workloads at present
                {"name": "elastic-2", "role": "datalake"},
                {"name": "elastic-3", "role": "datalake"},
                {"name": "elastic-4", "role": "datalake"},
                {"name": "elastic-5", "role": "datalake"},
                {"name": "kafka-1", "role": "queue"},  # automagical queue writes across queues
                {"name": "kafka-2", "role": "queue"},
                {"name": "kafka-3", "role": "queue"},
                {"name": "presentation-1", "role": "compute"}
            ]
            # Append additional instances if this isnt enough as is
        elif ingress_rate <= 1000:
            # Smallest amount of compute you can get away with
            required_instances = [
                {"name": "elastic-1", "role": "datalake"},
                {"name": "front-door-1", "role": "front-door"},
                {"name": "kafka-1", "role": "queue"}
            ]
        else:
            required_instances = [
                {"name": "front-door-1", "role": "front-door"},
                {"name": "normaliser-1", "role": "compute"},
                {"name": "elastic-1", "role": "datalake"},
                {"name": "elastic-2", "role": "datalake"},
                {"name": "elastic-3", "role": "datalake"},
                {"name": "kafka-1", "role": "queue"}
            ]
            logging.info("Scaled instances")
        return required_instances
