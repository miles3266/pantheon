import logging
import sys

import ZODB
import ZODB.FileStorage
import transaction

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


class DatabaseManager:

    def __init__(self):
        # TODO fix transaction issues
        self.storage = ZODB.FileStorage.FileStorage('pantheon.fs')  # TODO configure
        self.db = ZODB.DB(self.storage)
        connection = self.db.open()
        try:
            projects_database = connection.root.projects
        except AttributeError as uninitialised:
            logging.debug(uninitialised)
            logging.info("Core: DB seems to be empty, initalising")
            import BTrees.OOBTree
            connection.root.projects = BTrees.OOBTree.BTree()
            transaction.commit()
        connection.close()

    def get_connection(self):
        logging.info("DBM: Returned new connection")
        return self.db.open()

    def commit(self, connection):
        try:
            logging.info("DBM: attempting commit")
            transaction.commit()
        except Exception as e:
            logging.error(e)
        finally:
            connection.close()
