from flask import Flask
from flask_restful import Resource, Api
from pantheon.configuration.queues import Producers, Consumers
from pantheon.provisioners.capability import CapabilityManagerProvisioner
from pantheon.provisioners.core import CoreProvisioner
from pantheon.provisioners.core import CoreDetails
import logging
import sys
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

server = Flask(__name__)
api = Api(server)


class ApiRoot(Resource):
    def get(self):
        return {'message': 'this is the Pantheon Master root'}


api.add_resource(ApiRoot, '/')
api.add_resource(CoreProvisioner, '/core/provision')
api.add_resource(CoreDetails, '/core/<project_id>')
api.add_resource(CapabilityManagerProvisioner, '/capability-manager/provision')
# TODO configuration should probably be in an importable blueprint since its an
# entire component in its own right
api.add_resource(Producers, '/configuration/producers')
api.add_resource(Consumers, '/configuration/consumers')

if __name__ == '__main__':
    server.run(debug=True)
